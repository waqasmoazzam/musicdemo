package com.wmb.musicdemo.base.db.interfaces;

import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.List;

/**
 * Created by waqas on 03/09/2017.
 */

public interface RealmDB {

    void addFavouriteArtist(ArtistViewModel artistViewModel);

    void removeFavouriteArtist(int id);

    ArtistViewModel getArtistById(int id);

    List<ArtistViewModel> getFavouriteArtists();

    boolean isArtistFavourite(int id);
}
