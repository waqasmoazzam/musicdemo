package com.wmb.musicdemo.base.di.module;

import com.wmb.musicdemo.base.executor.implementation.ThreadExecutorImpl;
import com.wmb.musicdemo.base.executor.implementation.UIThreadImpl;
import com.wmb.musicdemo.base.executor.interfaces.Executor;
import com.wmb.musicdemo.base.executor.interfaces.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 02/09/2017.
 */

@Module
public class ExecutorModule {

    @Provides
    @Singleton
    public Executor provideExecutor(ThreadExecutorImpl threadExecutorImpl) {
        return threadExecutorImpl;
    }

    @Provides
    @Singleton
    public UIThread provideMainThread(UIThreadImpl uiThread) {
        return uiThread;
    }
}
