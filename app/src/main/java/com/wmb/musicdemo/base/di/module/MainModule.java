package com.wmb.musicdemo.base.di.module;

import android.content.Context;

import com.wmb.musicdemo.base.MusicDemoApplication;
import com.wmb.musicdemo.base.db.interfaces.RealmDB;
import com.wmb.musicdemo.base.di.annontations.PerActivity;
import com.wmb.musicdemo.base.ui.BaseActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 02/09/2017.
 */

@Module
public class MainModule {

    private MusicDemoApplication application;

    public MainModule(MusicDemoApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return application;
    }
}
