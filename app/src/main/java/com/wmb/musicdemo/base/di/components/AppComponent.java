package com.wmb.musicdemo.base.di.components;

import com.wmb.musicdemo.base.MusicDemoApplication;
import com.wmb.musicdemo.base.di.module.DataModule;
import com.wmb.musicdemo.base.di.module.ExecutorModule;
import com.wmb.musicdemo.base.di.module.LogicModule;
import com.wmb.musicdemo.base.di.module.MainModule;
import com.wmb.musicdemo.base.di.module.NetworkModule;
import com.wmb.musicdemo.base.di.module.PresenterModule;
import com.wmb.musicdemo.base.ui.BaseActivity;
import com.wmb.musicdemo.home.presenter.ArtistsPresenter;
import com.wmb.musicdemo.home.ui.activity.ArtistSearchActivity;
import com.wmb.musicdemo.home.ui.activity.FavouriteArtistsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by waqas on 02/09/2017.
 */

@Singleton
@Component(modules = {
        MainModule.class, ExecutorModule.class, LogicModule.class, NetworkModule.class, PresenterModule.class, DataModule.class
})
public interface AppComponent {
    void inject(MusicDemoApplication application);
    void inject(BaseActivity activity);
    void inject(ArtistSearchActivity activity);
    void inject(FavouriteArtistsActivity activity);

    ArtistsPresenter getArtistPresenter();
}
