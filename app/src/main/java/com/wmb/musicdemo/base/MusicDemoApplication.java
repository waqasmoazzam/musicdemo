package com.wmb.musicdemo.base;

import android.app.Application;

import com.wmb.musicdemo.BuildConfig;
import com.wmb.musicdemo.base.di.components.AppComponent;
import com.wmb.musicdemo.base.di.components.DaggerAppComponent;
import com.wmb.musicdemo.base.di.module.DataModule;
import com.wmb.musicdemo.base.di.module.ExecutorModule;
import com.wmb.musicdemo.base.di.module.LogicModule;
import com.wmb.musicdemo.base.di.module.MainModule;
import com.wmb.musicdemo.base.di.module.NetworkModule;
import com.wmb.musicdemo.base.di.module.PresenterModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Created by waqas on 02/09/2017.
 */

public class MusicDemoApplication extends Application {
    protected AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDIAppComponent();
        setupRealm();
        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void setupRealm() {
        Realm.init(this);
        RealmConfiguration config = new  RealmConfiguration.Builder().schemaVersion(1).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
    }

    private void setupDIAppComponent() {
        component = DaggerAppComponent.builder()
                .mainModule(new MainModule(this))
                .executorModule(new ExecutorModule())
                .networkModule(new NetworkModule())
                .presenterModule(new PresenterModule())
                .dataModule(new DataModule())
                .logicModule(new LogicModule())
                .build();
        component.inject(this);
    }

    public AppComponent getAppComponent() {
        return component;
    }
}
