package com.wmb.musicdemo.base.network;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by waqas on 02/09/2017.
 */

public class NetworkClient {
    public static final int INTERNAL_EXCEPTION = -1;
    private NetworkConfig config;

    public NetworkClient(NetworkConfig config) {
        this.config = config;
    }

    public <T> T getApi(Class<T> apiRest) {
        return config.getRetrofit().create(apiRest);
    }

    public <T> T execute(Call<T> call) throws NetworkException {
        Response<T> response;

        try {
            response = call.execute();
        } catch (Exception e) {
            throw new NetworkException(INTERNAL_EXCEPTION, e.getLocalizedMessage(), e);
        }

        if(response.isSuccessful()) {
            return response.body();
        } else {
            throw new NetworkException(response.code(), response.message(), null);
        }
    }
}

