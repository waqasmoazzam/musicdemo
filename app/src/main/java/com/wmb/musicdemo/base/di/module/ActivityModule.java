package com.wmb.musicdemo.base.di.module;

import android.content.Context;

import com.wmb.musicdemo.base.di.annontations.PerActivity;
import com.wmb.musicdemo.base.ui.BaseActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 02/09/2017.
 */

@Module
public class ActivityModule {

    private BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Context provideActivityContext() {
        return activity;
    }
}
