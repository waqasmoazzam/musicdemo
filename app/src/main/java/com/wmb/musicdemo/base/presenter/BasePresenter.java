package com.wmb.musicdemo.base.presenter;

/**
 * Created by waqas on 03/09/2017.
 */

public abstract class BasePresenter {
    public abstract void onDestroy();
}
