package com.wmb.musicdemo.base.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wmb.musicdemo.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by waqas on 02/09/2017.
 */

public class NetworkConfig {
    private Retrofit retrofit;

    public NetworkConfig(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public static class Builder {
        private static final String BASE_URL = BuildConfig.BASE_URL;
        private boolean debug = false;

        private Retrofit retrofit;

        public Builder() {}

        public Builder debug() {
            this.debug = BuildConfig.DEBUG_NETWORK_LOGS;
            return this;
        }

        public NetworkConfig build() {
            if(retrofit == null) {
                retrofit = createRetrofit(BASE_URL, debug);
            }
            return new NetworkConfig(retrofit);
        }

        private Retrofit createRetrofit(String baseURL, boolean debug) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            if(debug){
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(interceptor);
            }

            OkHttpClient client = builder.build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                    .create();

            return new Retrofit.Builder().baseUrl(baseURL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
    }
}
