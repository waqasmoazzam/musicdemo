package com.wmb.musicdemo.base.di.annontations;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.inject.Scope;

/**
 * Created by waqas on 02/09/2017.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {}
