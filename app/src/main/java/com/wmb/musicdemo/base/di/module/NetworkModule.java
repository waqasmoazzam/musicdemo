package com.wmb.musicdemo.base.di.module;

import com.wmb.musicdemo.base.network.NetworkConfig;
import com.wmb.musicdemo.home.api.artist.ArtistApiClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 03/09/2017.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    ArtistApiClient provideArtistApiClient() {
        NetworkConfig config = new NetworkConfig.Builder().debug().build();
        return new ArtistApiClient(config);
    }
}
