package com.wmb.musicdemo.base.common;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by waqas on 03/09/2017.
 */

public class Utils {
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showErrorDialog(Context context, CharSequence title, CharSequence message
            , CharSequence okButtonText, DialogInterface.OnClickListener okClickListener
            , CharSequence cancelButtonText, DialogInterface.OnClickListener cancelClickListener) {
        if(context == null) return;
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okButtonText, okClickListener)
                .setNegativeButton(cancelButtonText, cancelClickListener)
                .setCancelable(false)
                .show();
    }
}
