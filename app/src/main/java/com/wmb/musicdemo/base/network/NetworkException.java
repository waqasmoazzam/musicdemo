package com.wmb.musicdemo.base.network;

import dagger.Module;

/**
 * Created by waqas on 02/09/2017.
 */

public class NetworkException extends Exception{
    int code;
    String message;

    public NetworkException(int code, String message, Throwable throwable) {
        super(message, throwable);
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
