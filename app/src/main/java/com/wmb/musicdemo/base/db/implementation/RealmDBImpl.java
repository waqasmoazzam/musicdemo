package com.wmb.musicdemo.base.db.implementation;

import com.wmb.musicdemo.base.db.interfaces.RealmDB;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.List;

import io.realm.Realm;

/**
 * Created by waqas on 03/09/2017.
 */

public class RealmDBImpl implements RealmDB {

    private Realm getRealmInstance() {
        return Realm.getDefaultInstance();
    }

    @Override
    public void addFavouriteArtist(final ArtistViewModel artistViewModel) {
        if(artistViewModel != null) {
            getRealmInstance().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insertOrUpdate(artistViewModel);
                }
            });
        }
    }

    @Override
    public void removeFavouriteArtist(int id) {
        final ArtistViewModel artistViewModel = getArtistById(id);
        if(artistViewModel != null) {
            getRealmInstance().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    artistViewModel.deleteFromRealm();
                }
            });
        }
    }

    @Override
    public ArtistViewModel getArtistById(int id) {
        ArtistViewModel artistViewModel = getRealmInstance().where(ArtistViewModel.class).equalTo("id", id).findFirst();
        return artistViewModel;
    }

    @Override
    public List<ArtistViewModel> getFavouriteArtists() {
        Realm realm = getRealmInstance();
        List<ArtistViewModel> artistViewModels = realm.where(ArtistViewModel.class).findAll();
        if(artistViewModels != null) {
            artistViewModels = realm.copyFromRealm(artistViewModels);
        }
        return artistViewModels;
    }

    @Override
    public boolean isArtistFavourite(int id) {
        ArtistViewModel artistViewModel = getArtistById(id);
        return artistViewModel != null && artistViewModel.isFavorite();
    }
}
