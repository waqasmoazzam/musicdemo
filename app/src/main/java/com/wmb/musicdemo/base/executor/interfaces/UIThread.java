package com.wmb.musicdemo.base.executor.interfaces;

/**
 * Created by waqas on 31/08/2017.
 */

public interface UIThread {
    void post(final Runnable runnable);
}
