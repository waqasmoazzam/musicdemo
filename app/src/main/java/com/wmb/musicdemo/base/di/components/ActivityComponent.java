package com.wmb.musicdemo.base.di.components;

import android.content.Context;

import com.wmb.musicdemo.base.di.annontations.PerActivity;
import com.wmb.musicdemo.base.di.module.ActivityModule;

import dagger.Component;

/**
 * Created by waqas on 02/09/2017.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Context activity();
}
