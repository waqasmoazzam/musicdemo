package com.wmb.musicdemo.base.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.wmb.musicdemo.R;
import com.wmb.musicdemo.base.MusicDemoApplication;
import com.wmb.musicdemo.base.common.Utils;
import com.wmb.musicdemo.base.di.components.AppComponent;
import com.wmb.musicdemo.base.di.module.ActivityModule;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by waqas on 31/08/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable @BindView(R.id.toolbar) protected Toolbar toolbar;
    @Nullable @BindView(R.id.progress) protected ProgressBar progress;

    private Unbinder unbinder;

    protected abstract int getLayoutResource();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        injectViews();
        setToolBar(toolbar, "");
    }

    private void injectViews() {
        unbinder = ButterKnife.bind(this);
    }

    public void setToolBar(Toolbar toolbar, @Nullable String title) {
        if(toolbar == null) {
            return;
        }
        this.toolbar = toolbar;
        setSupportActionBar(toolbar);
        if (!TextUtils.isEmpty(title)) {
            setToolBarTitle(title); // to set a title, override in child activities
        }
    }

    public void setToolBarTitle(@NonNull String toolBarTitle) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(toolBarTitle);
        }
    }

    public  void showErrorDialog(Context context, String title, String message) {
        Utils.showErrorDialog(context, title, message,
                getString(android.R.string.ok), null, null, null);
    }

    public void showLoading(boolean show) {
        if(progress != null) {
            progress.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected ActivityModule getActivityModule(){
        return new ActivityModule(this);
    }

    protected AppComponent getApplicationComponent() {
        return ((MusicDemoApplication) getApplication()).getAppComponent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
