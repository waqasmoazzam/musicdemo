package com.wmb.musicdemo.base.di.module;

import com.wmb.musicdemo.base.db.implementation.RealmDBImpl;
import com.wmb.musicdemo.base.db.interfaces.RealmDB;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 03/09/2017.
 */

@Module
public class DataModule {

    @Provides
    public RealmDB provideRealmDBImpl() {
        return new RealmDBImpl();
    }
}
