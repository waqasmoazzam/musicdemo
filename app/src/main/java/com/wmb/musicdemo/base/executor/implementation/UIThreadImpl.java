package com.wmb.musicdemo.base.executor.implementation;

import android.os.Handler;
import android.os.Looper;

import com.wmb.musicdemo.base.executor.interfaces.UIThread;

import javax.inject.Inject;

/**
 * Created by waqas on 31/08/2017.
 */

public class UIThreadImpl implements UIThread {

    protected Handler handler;

    @Inject
    public UIThreadImpl() {
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
