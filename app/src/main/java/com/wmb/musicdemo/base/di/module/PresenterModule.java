package com.wmb.musicdemo.base.di.module;

import com.wmb.musicdemo.base.db.interfaces.RealmDB;
import com.wmb.musicdemo.home.domain.interfaces.GetArtist;
import com.wmb.musicdemo.home.presenter.ArtistsPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 03/09/2017.
 */

@Module
public class PresenterModule {

    @Provides
    ArtistsPresenter provideArtistPresenter(GetArtist getArtist, RealmDB realmDB) {
        return new ArtistsPresenter(getArtist, realmDB);
    }
}
