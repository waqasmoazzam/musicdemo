package com.wmb.musicdemo.base.di.module;

import com.wmb.musicdemo.home.domain.implementation.GetArtistInteractor;
import com.wmb.musicdemo.home.domain.interfaces.GetArtist;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by waqas on 03/09/2017.
 */

@Module
public class LogicModule {

    @Provides
    @Singleton
    public GetArtist provideGetArtistsInteractor(GetArtistInteractor interactor) {
        return interactor;
    }
}
