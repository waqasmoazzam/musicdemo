package com.wmb.musicdemo.home.domain.implementation;

import com.wmb.musicdemo.base.common.Utils;
import com.wmb.musicdemo.base.db.interfaces.RealmDB;
import com.wmb.musicdemo.base.executor.interfaces.Executor;
import com.wmb.musicdemo.base.executor.interfaces.Interactor;
import com.wmb.musicdemo.base.executor.interfaces.UIThread;
import com.wmb.musicdemo.base.network.NetworkClient;
import com.wmb.musicdemo.base.network.NetworkException;
import com.wmb.musicdemo.home.api.artist.ArtistApiClient;
import com.wmb.musicdemo.home.domain.interfaces.GetArtist;
import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.ArtistResponse;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by waqas on 03/09/2017.
 */

public class GetArtistInteractor implements GetArtist {

    Executor executor;
    UIThread uiThread;
    ArtistApiClient artistApiClient;
    ArtistResponse artistResponse;
    List<ArtistViewModel> artists;
    RealmDB realmDB;

    @Inject
    public GetArtistInteractor(Executor executor, UIThread uiThread, ArtistApiClient artistApiClient, RealmDB realmDB) {
        this.executor = executor;
        this.uiThread = uiThread;
        this.artistApiClient = artistApiClient;
        this.realmDB = realmDB;
    }

    @Override
    public void byName(final String name, final boolean isNext, final Callback callback) {
        executor.run(new Interactor() {
            @Override
            public void run() {
                try {
                    if(isNext) {
                        if(artistResponse.getNextUrl() == null) {
                            notifySuccess(artists, callback);
                            return;
                        }
                        artistResponse = artistApiClient.getArtists(artistResponse.getNextUrl());
                        artists.addAll(createViewModels(artistResponse.getAllArtists()));
                    } else {
                        artistResponse = artistApiClient.getArtistByName(name);
                        artists = createViewModels(artistResponse.getAllArtists());
                    }
                    notifySuccess(artists, callback);
                } catch (NetworkException e) {
                    e.printStackTrace();
                    if(e.getCode() == NetworkClient.INTERNAL_EXCEPTION) {
                        notifyError("MD cannot complete this request right now. Please try again later.", callback);
                    } else {
                        notifyError(e.getMessage(), callback);
                    }
                }
            }
        });
    }

    private List<ArtistViewModel> createViewModels(List<Artist> artistList) {
        List<ArtistViewModel> artistViewModels = new ArrayList<>();
        for(Artist artist : artistList) {
            artistViewModels.add(new ArtistViewModel(artist, realmDB.isArtistFavourite(artist.getId())));
        }
        return artistViewModels;
    }

    private void notifySuccess(final List<ArtistViewModel> artistViewModels, final Callback callback) {
        if(callback != null) {
            uiThread.post(new Runnable() {
                @Override
                public void run() {
                    callback.notifyArtists(artistViewModels);
                }
            });
        }
    }

    private void notifyError(final String errorMessage, final Callback callback) {
        if(callback != null) {
            uiThread.post(new Runnable() {
                @Override
                public void run() {
                    callback.notifyGetArtistsError(errorMessage);
                }
            });
        }
    }
}
