package com.wmb.musicdemo.home.domain.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by waqas on 02/09/2017.
 */

public class Track extends RealmObject {

    @SerializedName("id") private int id;
    @SerializedName("readable") private boolean readable;
    @SerializedName("title") private String title;
    @SerializedName("title_short") private String titleShort;
    @SerializedName("title_version") private String titleVersion;
    @SerializedName("isrc") private String isrc;
    @SerializedName("link") private String link;
    @SerializedName("duration") private int duration;
    @SerializedName("track_position") private int trackPosition;
    @SerializedName("disk_number") private int diskNumber;
    @SerializedName("rank") private int rank;
    @SerializedName("explicit_lyrics") private boolean explicitLyrics;
    @SerializedName("preview") private String preview;
    @SerializedName("artist") private Artist artist;
    @SerializedName("type") private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isReadable() {
        return readable;
    }

    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleShort() {
        return titleShort;
    }

    public void setTitleShort(String titleShort) {
        this.titleShort = titleShort;
    }

    public String getTitleVersion() {
        return titleVersion;
    }

    public void setTitleVersion(String titleVersion) {
        this.titleVersion = titleVersion;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTrackPosition() {
        return trackPosition;
    }

    public void setTrackPosition(int trackPosition) {
        this.trackPosition = trackPosition;
    }

    public int getDiskNumber() {
        return diskNumber;
    }

    public void setDiskNumber(int diskNumber) {
        this.diskNumber = diskNumber;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public boolean isExplicitLyrics() {
        return explicitLyrics;
    }

    public void setExplicitLyrics(boolean explicitLyrics) {
        this.explicitLyrics = explicitLyrics;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
