package com.wmb.musicdemo.home.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.wmb.musicdemo.R;
import com.wmb.musicdemo.base.common.Utils;
import com.wmb.musicdemo.base.ui.BaseActivity;
import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;
import com.wmb.musicdemo.home.presenter.ArtistsPresenter;
import com.wmb.musicdemo.home.ui.adapter.ArtistListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class FavouriteArtistsActivity extends BaseActivity {

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.rl_empty) RelativeLayout rlEmpty;

    @Inject ArtistsPresenter artistsPresenter;
    private ArtistListAdapter adapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_favourite_artists;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);

        setToolBarTitle(getString(R.string.title_favourite_activity));
        setupRecyclerView();
        fetchListFromDB();
    }

    private void fetchListFromDB() {
        List<ArtistViewModel> list = artistsPresenter.getFavouritArtists();
        if(list == null || list.isEmpty()) {
            rlEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            adapter.setArtistList(list);
            rlEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void setupRecyclerView() {
        adapter = new ArtistListAdapter(true, itemClickListener);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ArtistViewModel avm = (ArtistViewModel) v.getTag();
            Artist artist = avm.getArtist();
            if(v.getId() == R.id.checkbox_favourite) {
                if(avm.isFavorite()) {
                    artistsPresenter.addFavouriteArtist(avm);
                } else {
                    artistsPresenter.removeFavouriteArtist(artist.getId());
                }

//                adapter.removeItem(avm);
            } else {
                // TODO: show tracks for this artist.
                Toast.makeText(v.getContext(), "Total fans: " + artist.getNbFan(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ArtistSearchActivity.REQUEST_CODE) {
            fetchListFromDB();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.favourite_music_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.mi_search) {
            if(!Utils.isNetworkAvailable(this)) {
                Toast.makeText(this, getString(R.string.error_message_no_network), Toast.LENGTH_LONG).show();
                return false;
            }
            ArtistSearchActivity.startForResult(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
