package com.wmb.musicdemo.home.presenter;

import com.wmb.musicdemo.base.db.interfaces.RealmDB;
import com.wmb.musicdemo.base.network.NetworkException;
import com.wmb.musicdemo.base.presenter.BasePresenter;
import com.wmb.musicdemo.home.domain.interfaces.GetArtist;
import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by waqas on 03/09/2017.
 */

public class ArtistsPresenter extends BasePresenter implements GetArtist.Callback {
    GetArtist getArtist;
    RealmDB realmDB;
    View.Callback callback;

    @Inject
    public ArtistsPresenter(GetArtist getArtist, RealmDB realmDB) {
        this.getArtist = getArtist;
        this.realmDB = realmDB;
    }

    public void searchArtistByName(String name, boolean isNext, View.Callback callback) {
        this.callback = callback;
        if(callback != null) {
            callback.showLoading(true);
        }
        getArtist.byName(name, isNext, this);
    }

    public void addFavouriteArtist(ArtistViewModel artistViewModel) {
        realmDB.addFavouriteArtist(artistViewModel);
    }

    public void removeFavouriteArtist(int id) {
        realmDB.removeFavouriteArtist(id);
    }

    public boolean isArtistFavourite(int id) {
        return realmDB.isArtistFavourite(id);
    }

    public List<ArtistViewModel> getFavouritArtists() {
        return realmDB.getFavouriteArtists();
    }

    @Override
    public void notifyArtists(List<ArtistViewModel> artistViewModels) {
        if(callback != null) {
            callback.showLoading(false);
            callback.onArtistSearchResult(artistViewModels);
        }
    }

    @Override
    public void notifyGetArtistsError(String errorMessage) {
        if(callback != null) {
            callback.showLoading(false);
            callback.onArtistSearchResultError(errorMessage);
        }
    }

    @Override
    public void onDestroy() {
        this.callback = null;
    }

    public interface View {
        interface Callback {
            void onArtistSearchResult(List<ArtistViewModel> artistViewModels);
            void onArtistSearchResultError(String errorMessage);
            void showLoading(boolean show);
        }
    }
}
