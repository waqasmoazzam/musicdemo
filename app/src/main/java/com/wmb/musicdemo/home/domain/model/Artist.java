package com.wmb.musicdemo.home.domain.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by waqas on 02/09/2017.
 */

public class Artist extends RealmObject {

    @PrimaryKey @SerializedName("id") private int id;
    @SerializedName("name") private String name;
    @SerializedName("link") private String link;
    @SerializedName("picture") private String picture;
    @SerializedName("picture_small") private String pictureSmall;
    @SerializedName("picture_medium") private String pictureMedium;
    @SerializedName("picture_big") private String pictureBig;
    @SerializedName("picture_xl") private String pictureXl;
    @SerializedName("nb_album") private int nbAlbum;
    @SerializedName("nb_fan") private int nbFan;
    @SerializedName("radio") private boolean radio;
    @SerializedName("tracklist") private String tracklist;
    @SerializedName("type") private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPictureSmall() {
        return pictureSmall;
    }

    public void setPictureSmall(String pictureSmall) {
        this.pictureSmall = pictureSmall;
    }

    public String getPictureMedium() {
        return pictureMedium;
    }

    public void setPictureMedium(String pictureMedium) {
        this.pictureMedium = pictureMedium;
    }

    public String getPictureBig() {
        return pictureBig;
    }

    public void setPictureBig(String pictureBig) {
        this.pictureBig = pictureBig;
    }

    public String getPictureXl() {
        return pictureXl;
    }

    public void setPictureXl(String pictureXl) {
        this.pictureXl = pictureXl;
    }

    public int getNbAlbum() {
        return nbAlbum;
    }

    public void setNbAlbum(int nbAlbum) {
        this.nbAlbum = nbAlbum;
    }

    public int getNbFan() {
        return nbFan;
    }

    public void setNbFan(int nbFan) {
        this.nbFan = nbFan;
    }

    public boolean isRadio() {
        return radio;
    }

    public void setRadio(boolean radio) {
        this.radio = radio;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
