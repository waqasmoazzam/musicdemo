package com.wmb.musicdemo.home.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wmb.musicdemo.R;
import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by waqas on 03/09/2017.
 */

public class ArtistListAdapter extends RecyclerView.Adapter<ArtistListAdapter.ViewHolder> {

    protected class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        @BindView(R.id.image_artist_pic) ImageView imageArtistPicture;
        @BindView(R.id.text_artist_name) TextView textArtistName;
        @BindView(R.id.checkbox_favourite) CheckBox checkBoxFavourite;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void render(final ArtistViewModel artistVM) {
            view.setTag(artistVM);

            final Artist artist = artistVM.getArtist();
            textArtistName.setText(artist.getName());
            if(loadHighResPicture) {
                Picasso.with(view.getContext()).load(artist.getPictureMedium()).into(imageArtistPicture);
            } else {
                Picasso.with(view.getContext()).load(artist.getPicture()).into(imageArtistPicture);
            }

            view.setOnClickListener(itemClickListener);
            checkBoxFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    artistVM.setFavorite(!artistVM.isFavorite());
                    v.setTag(artistVM);
                    itemClickListener.onClick(v);
                }
            });
            checkBoxFavourite.setChecked(artistVM.isFavorite());
        }
    }

    private List<ArtistViewModel> list;
    private View.OnClickListener itemClickListener;
    private boolean loadHighResPicture;

    public ArtistListAdapter(boolean loadHighResPicture, View.OnClickListener itemClickListener) {
        this.loadHighResPicture = loadHighResPicture;
        this.itemClickListener = itemClickListener;
    }

    public void setArtistList(List<ArtistViewModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void removeItem(ArtistViewModel avm) {
        if(list.contains(avm)) {
            list.remove(avm);
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.render(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }
}
