package com.wmb.musicdemo.home.api.artist;

import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.ArtistResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by waqas on 03/09/2017.
 */

public interface ArtistRestApi {
    @GET("/search/artist/")
    Call<ArtistResponse> getArtistsByName(@Query("q") String name);

    @GET
    Call<ArtistResponse> getArtists(@Url String url);
}
