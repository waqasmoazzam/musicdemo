package com.wmb.musicdemo.home.domain.model.viewmodel;

import com.wmb.musicdemo.home.domain.model.Artist;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by waqas on 03/09/2017.
 */

public class ArtistViewModel extends RealmObject {
    @PrimaryKey private int id;
    private Artist artist;
    private boolean isFavorite;

    public ArtistViewModel() {
    }

    public ArtistViewModel(Artist artist, boolean isFavorite) {
        id = artist.getId();
        this.artist = artist;
        this.isFavorite = isFavorite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
