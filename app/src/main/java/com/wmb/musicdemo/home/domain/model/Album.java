package com.wmb.musicdemo.home.domain.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by waqas on 02/09/2017.
 */

public class Album extends RealmObject {
    @SerializedName("id") private int id;
    @SerializedName("title") private String title;
    @SerializedName("link") private String link;
    @SerializedName("cover") private String cover;
    @SerializedName("cover_small") private String coverSmall;
    @SerializedName("cover_medium") private String coverMedium;
    @SerializedName("cover_big") private String coverBig;
    @SerializedName("cover_xl") private String coverXl;
    @SerializedName("genre_id") private int genreId;
    @SerializedName("fans") private int fans;
    @SerializedName("release_date") private String releaseDate;
    @SerializedName("record_type") private String recordType;
    @SerializedName("tracklist") private String tracklist;
    @SerializedName("explicit_lyrics") private boolean explicitLyrics;
    @SerializedName("type") private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCoverSmall() {
        return coverSmall;
    }

    public void setCoverSmall(String coverSmall) {
        this.coverSmall = coverSmall;
    }

    public String getCoverMedium() {
        return coverMedium;
    }

    public void setCoverMedium(String coverMedium) {
        this.coverMedium = coverMedium;
    }

    public String getCoverBig() {
        return coverBig;
    }

    public void setCoverBig(String coverBig) {
        this.coverBig = coverBig;
    }

    public String getCoverXl() {
        return coverXl;
    }

    public void setCoverXl(String coverXl) {
        this.coverXl = coverXl;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getFans() {
        return fans;
    }

    public void setFans(int fans) {
        this.fans = fans;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }

    public boolean isExplicitLyrics() {
        return explicitLyrics;
    }

    public void setExplicitLyrics(boolean explicitLyrics) {
        this.explicitLyrics = explicitLyrics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
