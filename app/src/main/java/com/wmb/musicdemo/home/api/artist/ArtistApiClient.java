package com.wmb.musicdemo.home.api.artist;

import com.wmb.musicdemo.base.network.NetworkClient;
import com.wmb.musicdemo.base.network.NetworkConfig;
import com.wmb.musicdemo.base.network.NetworkException;
import com.wmb.musicdemo.home.domain.model.ArtistResponse;

import retrofit2.Call;

/**
 * Created by waqas on 03/09/2017.
 */

public class ArtistApiClient extends NetworkClient {
    ArtistRestApi api;

    public ArtistApiClient(NetworkConfig config) {
        super(config);
        this.api = getApi(ArtistRestApi.class);
    }

    public ArtistResponse getArtistByName(String name) throws NetworkException {
        Call<ArtistResponse> call = api.getArtistsByName(name);
        ArtistResponse artistResponse = execute(call);
        return artistResponse;
    }

    public ArtistResponse getArtists(String url) throws NetworkException {
        Call<ArtistResponse> call = api.getArtists(url);
        ArtistResponse artistResponse = execute(call);
        return artistResponse;
    }
}
