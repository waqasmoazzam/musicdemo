package com.wmb.musicdemo.home.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.wmb.musicdemo.R;
import com.wmb.musicdemo.base.common.Utils;
import com.wmb.musicdemo.base.ui.BaseActivity;
import com.wmb.musicdemo.home.domain.model.Artist;
import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;
import com.wmb.musicdemo.home.presenter.ArtistsPresenter;
import com.wmb.musicdemo.home.ui.adapter.ArtistListAdapter;
import com.wmb.musicdemo.home.ui.custom.EndlessRecyclerOnScrollListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class ArtistSearchActivity extends BaseActivity implements ArtistsPresenter.View.Callback {

    public static final int REQUEST_CODE = 1010;
    private static final int MESSAGE_SEARCH_TEXT_CHANGED = 100;
    private static final int AUTO_SEARCH_DELAY = 500;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @Inject ArtistsPresenter artistsPresenter;
    private ArtistListAdapter adapter;

    public static void startForResult(Context context) {
        Intent intent = new Intent(context, ArtistSearchActivity.class);
        ((BaseActivity)context).startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_artist_search;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);
        setToolBarTitle(getString(R.string.title_search_activity));
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        adapter = new ArtistListAdapter(false, itemClickListener);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore() {
                artistsPresenter.searchArtistByName("", true, ArtistSearchActivity.this);
            }
        });
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ArtistViewModel avm = (ArtistViewModel) v.getTag();
            Artist artist = avm.getArtist();
            if(v.getId() == R.id.checkbox_favourite) {
                if(avm.isFavorite()) {
                    // TODO: store in DB
                    artistsPresenter.addFavouriteArtist(avm);
                } else if(!avm.isFavorite()) {
                    // TODO: remove from DB.
                    artistsPresenter.removeFavouriteArtist(artist.getId());
                }
            } else {
                // TODO: show tracks for this artist.
            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_music_menu, menu);

        setupSearchView(menu.findItem(R.id.mi_search));

        return true;
    }

    private void setupSearchView(MenuItem miSearch) {
        if(miSearch == null) {
            return;
        }
        miSearch.setVisible(true);
        final SearchView searchView = (SearchView) miSearch.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                String query = searchView.getQuery().toString();
                searchHandler.removeMessages(MESSAGE_SEARCH_TEXT_CHANGED);
                searchHandler.sendMessageDelayed(searchHandler.obtainMessage(MESSAGE_SEARCH_TEXT_CHANGED, query), AUTO_SEARCH_DELAY);
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(miSearch, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                setResult(RESULT_OK);
                finish();
                return false;
            }
        });

        miSearch.expandActionView();
    }

    private final Handler searchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String query = (String) msg.obj;
            if(!Utils.isNetworkAvailable(ArtistSearchActivity.this)) {
                searchHandler.removeMessages(MESSAGE_SEARCH_TEXT_CHANGED);
                showErrorDialog(ArtistSearchActivity.this, getString(R.string.error_title), getString(R.string.error_message_no_network));
                return;
            }
            if(!query.isEmpty()) {
                search(query);
            }
        }
    };

    private void search(String query) {
        artistsPresenter.searchArtistByName(query, false, this);
    }

    @Override
    public void onArtistSearchResult(List<ArtistViewModel> artistViewModels) {
        Timber.i("onArtistSearchResult: " + artistViewModels.size());
        adapter.setArtistList(artistViewModels);
        Utils.hideKeyboard(this, recyclerView);
    }

    @Override
    public void onArtistSearchResultError(String errorMessage) {
        Timber.i("onArtistSearchResultError: " + errorMessage);
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        Utils.hideKeyboard(this, recyclerView);
    }

    @Override
    public void showLoading(boolean show) {
        super.showLoading(show);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        artistsPresenter.onDestroy();
    }
}
