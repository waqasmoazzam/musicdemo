package com.wmb.musicdemo.home.domain.interfaces;

import com.wmb.musicdemo.home.domain.model.viewmodel.ArtistViewModel;

import java.util.List;

/**
 * Created by waqas on 02/09/2017.
 */

public interface GetArtist {
    interface Callback {
        void notifyArtists(List<ArtistViewModel> artistViewModels);
        void notifyGetArtistsError(String errorMessage);
    }
    void byName(String name, boolean isNext, Callback callback);
}
