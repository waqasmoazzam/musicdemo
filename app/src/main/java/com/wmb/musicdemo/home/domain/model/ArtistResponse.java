package com.wmb.musicdemo.home.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by waqas on 03/09/2017.
 */

public class ArtistResponse {
    @SerializedName("data") List<Artist> allArtists;
    @SerializedName("total") int total;
    @SerializedName("next") String nextUrl;

    public List<Artist> getAllArtists() {
        return allArtists;
    }

    public void setAllArtists(List<Artist> allArtists) {
        this.allArtists = allArtists;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }
}
